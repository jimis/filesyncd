pub static USAGE: &str = "
Usage is possible in two modes, either \"source\" or \"destination\" mode:


Source mode: Send contents of directory to the given address.
        This is the default mode.

  filesyncd [--mode source] /path/to/dir 1.2.3.4[:9493]


Destination mode: Listen to the given address (127.0.0.1 by default),
        receive files, write them to directory.

  filesyncd --mode destination /path/to/dir [1.2.3.4[:9493]]


Options:
    -h  --help     Print this help message and exit
    -M  --mode     { d|dst|dest|destination|s|src|source }
    -v  --verbose  increase verbosity; use twice for more.
";

pub static FILELIST_REQUEST:   &[u8] = b"GET /api/filelist\r\n";
pub static WRITE_FILE_REQUEST: &[u8] = b"POST /api/write_file\r\n";

pub static OK_RESPONSE:        &[u8] = b"HTTP/1.0 200 OK\r\n";
