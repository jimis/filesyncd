#![allow(unused_macros)]
#![allow(unused_imports)]

use crate::public::{*};

// NO LOGGING PERMITTED HERE, logger has not been initialised
// extern crate env_logger;
// use log::{error, warn, info, debug, trace};                     // macro

extern crate serde;
extern crate serde_json;
use serde::{Serialize,Deserialize};

use std::io::Read;
use std::io::Write;
use std::io::BufRead;
use std::iter::Iterator;

use std::fs;
use std::fs::File;
use std::path;
use std::path::Path;
use std::path::PathBuf;
use std::net;
use std::net::TcpListener;
use std::net::TcpStream;
use std::net::SocketAddr;
use std::process::exit;
use std::io;
use std::io::BufReader;
use std::collections::HashMap;
use std::error::Error;
use std::ffi::OsString;



#[derive(Debug, Serialize, Deserialize)]
pub enum ModeEnum {Src, Dst}

#[derive(Debug)]
pub struct Config {
    pub argv0:      String,
    pub mode:       ModeEnum,
    pub address:    net::IpAddr,
    pub port:       u16,
    pub verbosity:  usize,
    pub shared_dir: PathBuf,
}
impl Config
{

    /**
    TODO TEST

    target/debug/filesyncd  --mode d  127.0.0.1 1234
    target/debug/filesyncd  --mode d  127.0.0.1:1234
    target/debug/filesyncd  --mode s  127.0.0.1:1234
    target/debug/filesyncd  127.0.0.1:1234   (should be equal to the previous)

    */
    pub fn new(args: &[String]) -> Config
    {
        let mut argv0: Option<String>        = None;
        let mut mode: Option<ModeEnum>       = None;
        let mut address: Option<net::IpAddr> = None;
        let mut shared_dir:  Option<PathBuf> = None;
        let mut port     : u16    = 9493;
        let mut verbosity: usize  = 0;

        let mut expecting_mode: bool = false;
        let mut got_directory:  bool = false;
        let mut got_address:    bool = false;

        for arg in args
        {
            println!("Parsing argument: {}", arg);

            if ! argv0.is_some()
            {
                argv0     = Some(arg.clone());
            }
            else if expecting_mode
            {
                expecting_mode = false;
                match arg.as_ref()
                {
                    "destination" | "dest" | "dst" | "d"  =>
                        mode = Some(ModeEnum::Dst),
                    "source" | "src" | "s"  =>
                        mode = Some(ModeEnum::Src),
                    _ =>
                        panic!("Expected --mode destination OR source"),
                }
            }
            else if arg == "--mode"
                ||  arg == "-M"
            {
                expecting_mode = true;
            }
            else if arg == "-h"
                ||  arg == "--help"
            {
                exit_usage(0);
            }
            else if arg == "--verbose"
                ||  arg == "-v"
            {
                verbosity += 1;
            }
            else if arg.starts_with("-")
            {
                panic!("Uknown option: {}", arg);
            }
            else if ! got_directory
            {
                shared_dir = Some(arg.into());
                got_directory = true;
            }
            // got directory, optionally expecting address and port
            else if ! got_address
            {
                let mut addr_string = &arg[..];
                // If the string includes a port, then split into two strings

                // find last ':', check that only numbers follow, check that
                // if ':' can be found before, then address is in brackets, or
                // else it is not a port but the last part of an IPv6 address.
                let last_colon = arg.rfind(':');
                if let Some(n) = last_colon
                {
                    let before_colon = &arg[..n];
                    let after_colon  = &arg[n+1..];
                    if after_colon.matches(char::is_numeric).count()
                        == after_colon.len()
                    {
                        if before_colon.matches(':').count() == 0
                        {
                            // IPv4 with port
                            port = after_colon.parse()
                                .expect("Error parsing port number");
                            addr_string = before_colon;
                        }
                        else if before_colon.starts_with('[')
                            &&  before_colon.ends_with (']')
                        {
                            // IPv6 with port
                            port = after_colon.parse()
                                .expect("Error parsing port number");
                            addr_string = before_colon;
                        }
                    }
                }

                address = Some(addr_string.parse()
                               .expect("Error parsing address"));

                got_address = true;
            }
            else
            {
                panic!("Too many arguments!");
            }
        }

        return Config {
            argv0:   argv0.expect("argv[0] is missing!!?"),
            // Client mode is the default
            mode:    mode.unwrap_or(ModeEnum::Src),
            address: address.expect("No address was provided in client mode"),
            port:    port,
            verbosity:  verbosity,
            shared_dir: shared_dir.unwrap(),
        };
    }
}

pub fn exit_usage(exitcode: i32)
{
    println!("{}\n", USAGE);

    std::process::exit(exitcode);
}

