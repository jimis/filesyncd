#![allow(unused_macros)]
#![allow(unused_imports)]

mod public;
use public::{*};

mod utils;
use utils::SimpleFileType;
use utils::FileDetails;
use utils::calculate_filelist;
use utils::is_path_safe;
use utils::get_headers;

mod config;
use config::Config;
use config::ModeEnum;

mod src_mode;
use src_mode::run_source_mode;

mod dst_mode;
use dst_mode::run_destination_mode;


extern crate log;
extern crate env_logger;
use log::{error, warn, info, debug, trace};                     // macro

extern crate serde;
extern crate serde_json;



fn initialize_env_logger(verbosity: usize)
{
    //env_logger::init();
    // We use env_logger::Builder to init with custom verbosity.

    // levels ERROR and WARN are always enabled
    let level = match verbosity {
        0 => log::LevelFilter::Debug,
        1 => log::LevelFilter::Trace,
        _ => log::LevelFilter::Trace,
    };

    let mut builder = env_logger::Builder::new();
    builder.filter(None, level);
    if std::env::var("RUST_LOG").is_ok() {
       builder.parse(&std::env::var("RUST_LOG").unwrap());
    }
    builder.init();
}

fn main()
{
    let args: Vec<String> = std::env::args().collect();

    let config = Config::new(&args);

    initialize_env_logger(config.verbosity);

    match config.mode
    {
        ModeEnum::Src => run_source_mode(&config),
        ModeEnum::Dst => run_destination_mode(&config),
    }
}
