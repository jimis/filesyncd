#![allow(unused_macros)]
#![allow(unused_imports)]

use crate::utils::SimpleFileType;
use crate::utils::FileDetails;
use crate::utils::calculate_filelist;
use crate::utils::is_path_safe;
use crate::utils::get_headers;
use crate::config::Config;
use crate::public::{*};

extern crate log;
extern crate env_logger;
use log::{error, warn, info, debug, trace};                     // macro

extern crate serde;
extern crate serde_json;
use serde::{Serialize,Deserialize};

use std::io::Read;
use std::io::Write;
use std::io::BufRead;
use std::iter::Iterator;
use std::os::unix::fs::PermissionsExt;

use std::fs;
use std::fs::File;
use std::path;
use std::path::Path;
use std::path::PathBuf;
use std::time;
use std::net;
use std::net::TcpListener;
use std::net::TcpStream;
use std::net::SocketAddr;
use std::process::exit;
use std::io;
use std::io::BufReader;
use std::collections::HashMap;
use std::error::Error;
use std::ffi::OsString;



pub fn run_source_mode(config: &Config)
{
    trace!("Entering source mode with source directory: {}",
           config.shared_dir.to_string_lossy());

    // TODO loop every 60 seconds
    //{

    let addr = SocketAddr::new(config.address, config.port);

        // TODO only if stream has been closed
    let mut stream = match TcpStream::connect(addr)
    {
        Err(e) =>  { error!("Failed to connect to {} ({})", addr, e);
                     exit(1); },
        Ok(s)  => s,
    };
    info!("Connected to: {}", addr);
    debug!("Socket info: {:?}", stream);

    stream.write_all(FILELIST_REQUEST).unwrap();

    // Borrow the TcpStream to do buffered reading
    let mut stream_buf   = BufReader::new(&mut stream);

    // Receive DESTINATION filelist
    let dst_filedetails: Vec<FileDetails> =
        get_filelist(&mut stream_buf).unwrap();

    // Give back the borrowed reference to Tcpstream
    let stream: &mut TcpStream = stream_buf.into_inner();

    // Calculate SOURCE filelist
    let src_filedetails: Vec<FileDetails> =
        calculate_filelist(&config.shared_dir).unwrap();

    // Loop over SRC filelist and find missing and older files in DST filelist
    for src_fd in src_filedetails
    {
        match dst_filedetails.iter().find(|&dst_fd|
                                          dst_fd.path == src_fd.path)
        {
            // File does not exist at DST
            None =>
            {
                if src_fd.kind == SimpleFileType::Dir
                {
                    send_write_file(config, stream, &src_fd,
                                    "directory does not exist").unwrap();
                }
                else
                {
                    send_write_file(config, stream, &src_fd,
                                    "file does not exist").unwrap();
                }

            },
            // File exists at DST
            Some(dst_fd) =>
            {
                if src_fd.kind == SimpleFileType::Dir
                {
                    trace!("directory exists  at destination, skipping: {:?}",
                           src_fd.path);
                }
                // Is the file at SRC newer than DST?
                else if src_fd.mtime > dst_fd.mtime
                {
                    send_write_file(config, stream, &src_fd, "file older").unwrap();
                }
                // Or maybe the files are obviously different (sizes differ)
                else if src_fd.size != dst_fd.size
                {
                    send_write_file(config, stream, &src_fd, "sizes differ").unwrap();
                }
                else
                {
                    trace!("file is fresh at destination, skipping: {:?}",
                           src_fd.path);
                }

            // TODO what if a directory exists as file? Or the opposite?
            },
        }
    }

    info!("DONE, closing connection!");
}


/**
 * Helper function that receives the JSON file listing response to a
 * FILELIST_REQUEST, and returns it in a new Vec.
 */
fn get_filelist<T>(stream_buf: &mut BufReader<T>)
    ->  Result<Vec<FileDetails>, Box<dyn Error>>
    where T: Read
{
    let headers = get_headers(stream_buf)?;

    let body_size: u64  = headers.get("content-length")
        .ok_or("missing content-length header")?
        .parse()?;

    let filedetails: Vec<FileDetails> =
        serde_json::from_reader(stream_buf.take(body_size))?;

    return Ok(filedetails);
}

/**
 *  Send a WRITE_FILE_REQUEST.
 */
fn send_write_file(config: &Config, stream: &mut TcpStream,
                   fd: &FileDetails, reason: &str)
    -> Result<(), std::io::Error>
{
    trace!("send_WRITE_FILE of size {}: {:?}  (reason: {})",
           fd.size, fd.path, reason);

    // Send headers followed by the right number of bytes in the body
    stream.write_all(WRITE_FILE_REQUEST)?;

    // Serialize filedetails to JSON
    let sj = serde_json::to_string(&fd)?;

    let body_size =   if fd.kind != SimpleFileType::Reg { 0 } else { fd.size };

    stream.write_all(b"Content-Type: application/octet-stream\r\n")?;
    write!(stream, "Content-Length: {}\r\n", body_size)?;
    write!(stream, "X-File-Metadata: {}\r\n", sj)?;
    stream.write_all(b"\r\n")?;

    if fd.kind == SimpleFileType::Reg
    {
        // Create a Reader from the file of exactly the size we saw earlier. If
        // the file became smaller in the meantime, if should fail.
        let path = config.shared_dir.join(&fd.path);
        let mut reader = File::open(path)?.take(fd.size);

        io::copy(&mut reader, stream)?;
    }

    //TODO Read OK_RESPONSE

    trace!("send_WRITE_FILE: DONE");
    Ok(())
}
