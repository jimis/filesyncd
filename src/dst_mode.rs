#![allow(unused_macros)]
#![allow(unused_imports)]

use crate::utils::SimpleFileType;
use crate::utils::FileDetails;
use crate::utils::calculate_filelist;
use crate::utils::is_path_safe;
use crate::utils::get_headers;
use crate::config::Config;
use crate::public::{*};

extern crate log;
extern crate env_logger;
use log::{error, warn, info, debug, trace};                     // macro

extern crate serde;
extern crate serde_json;
use serde::{Serialize,Deserialize};

use std::io::Read;
use std::io::Write;
use std::io::BufRead;
use std::iter::Iterator;
use std::os::unix::fs::PermissionsExt;

use std::fs;
use std::fs::File;
use std::path;
use std::path::Path;
use std::path::PathBuf;
use std::time;
use std::net;
use std::net::TcpListener;
use std::net::TcpStream;
use std::net::SocketAddr;
use std::process::exit;
use std::io;
use std::io::BufReader;
use std::collections::HashMap;
use std::error::Error;
use std::ffi::OsString;



/**
 *  Run constantly, listen for incoming connections, serve them.
 */
pub fn run_destination_mode(config: &Config)
{
    trace!("Entering destination mode with destination directory: {}",
           config.shared_dir.to_string_lossy());

    let listener = TcpListener::bind((config.address, config.port)).unwrap();

    info!("Listening on {}:{}", config.address, config.port);

    loop
    {
        // accept()
        let (stream, peer_addr) = match listener.accept()  {
            Err(e) =>
            {
                warn!("Error accepting connection: {}", e);
                continue;
            },
            Ok(tuple)  => tuple,
        };
        info!("Accepted connection from: {}", peer_addr);
        debug!("Socket info: {:?}", stream);

        // by_ref() is needed to keep the connection open
        serve(config, stream).unwrap_or_else(
            |e| warn!("Error while serving request: {}", e));
    }
}


fn serve(config: &Config, stream: TcpStream)
    -> Result<(), Box<dyn Error>>
{
    let mut buf: Vec<u8>  = Vec::new();

    // It is IMPORTANT to have this BufReader outside of the loop, even if
    // this means fighting the borrow checker; if it's defined at the
    // beginning of the loop, cached incoming data is being discarded and it
    // seems like socket is receiving garbage.
    let mut stream_buf    = BufReader::new(&stream);

    // Loop here to consume all of client's requests, until peer closes the
    // connection or an error occurs.
    loop
    {

        let _next_newline   = stream_buf.read_until(b'\n', &mut buf).unwrap();
        //TODO        .and(buf[buf.len()-2] == b'\r');

        if buf.len() == 0
        {
            debug!("Peer closed socket, going back to main loop");
            break;
        }
        else if buf.starts_with(FILELIST_REQUEST)
        {
            handle_filelist_request(config, &mut stream_buf)?;
        }
        else if buf.starts_with(WRITE_FILE_REQUEST)
        {
            handle_write_file_request(config, &mut stream_buf)?;
        }
        else
        {
            debug!("BAD REQUEST of len {}: {}",
                   buf.len(), String::from_utf8_lossy(&buf));
            warn!("Closing connection and going back to main loop!");

            Err("Bad request")?;                            // This always returns
        }
    buf.clear();
}

    Ok(())
}

fn handle_filelist_request(config: &Config,
                           bufreader: &mut BufReader<&TcpStream>)
    -> Result<(), Box<dyn Error>>
{
    trace!("Received FILELIST_REQUEST");

    // Do a recursive listing of shared_dir and
    // store results in vec of FileDetails
    let filedetails: Vec<FileDetails> =
        calculate_filelist(&config.shared_dir)?;

    // Serialize filedetails as JSON
    let sj = serde_json::to_string_pretty(&filedetails).unwrap();

    let sj_bytes: &[u8] = sj.as_bytes();

    // Temporarily borrow the stream in order to write
    let stream_writer = bufreader.get_mut();

    stream_writer.write_all(OK_RESPONSE)?;
    write!(stream_writer, "Content-Length: {}\r\n", sj_bytes.len())?;
    stream_writer.write_all(b"Content-Type: application/json\r\n")?;
    stream_writer.write_all(b"\r\n")?;

    // Finally send the JSON file listing
    stream_writer.write_all(sj_bytes)?;

    debug!("Sent JSON file listing, {} files, {} bytes long",
           filedetails.len(), sj_bytes.len());
    Ok(())
}

fn handle_write_file_request(config: &Config,
                             mut bufreader: &mut BufReader<&TcpStream>)
    -> Result<(), Box<dyn Error>>
{
    trace!("Received WRITE_FILE_REQUEST");

    let headers = get_headers(&mut bufreader)?;

    let body_size: u64  = headers.get("content-length")
        .ok_or("missing Content-Length header")?
        .parse()?;

    let fd_header = headers.get("x-file-metadata")
        .ok_or("missing X-File-Metadata header")?;

    // Deserialise file metadata
    let fd: FileDetails = serde_json::from_slice(fd_header.as_bytes())?;

    trace!("Received Content-Length: {}", body_size);

    let s = fd.path.to_string_lossy();             // for logging purposes

    // Verify path safety

    if ! is_path_safe(&fd.path)
    {
        warn!("Skipping path containing dangerous components: {}", s);
        // TODO respond HTTP ERR
        Err("Dangerous path")?;                         // This always returns
    }

    // Warning, this join operation replaces the 1st component, if the 2nd
    // is absolute or C: etc. We already checked path is safe though.
    let abs_path = config.shared_dir.join(&fd.path);

    // Assert that it is under the shared_dir
    if ! abs_path.starts_with(&config.shared_dir)
    {
        warn!("Skipping path that escaped the target directory: {}", s);
        // This always returns
        Err("Dangerous path tried to escape shared directory")?;
    }

    match &fd.kind
    {
    SimpleFileType::Sym =>   {
        warn!("Skipping file of unsupported type symlink: {}", s);
    },
    SimpleFileType::Other => {
        warn!("Skipping file of unknown type: {}", s);
    },
    SimpleFileType::Dir => {

        match fs::symlink_metadata(&abs_path)
        {
        Err(err) => match err.kind()
        {
            io::ErrorKind::NotFound =>
            {
                debug!("MKDIR: {:?}", abs_path);
                fs::create_dir(abs_path)?;
            }
            _ => { warn!("Skipping writing file {:?} (Error: {:?})",
                         abs_path, err);
            },
        },
        Ok(metadata) =>
        {
            if ! metadata.is_dir()
            {
                warn!("Skipping making directory {:?} because it
                    already exists as file", abs_path);
            }
            else
            {
                trace!("Directory already exists: {:?}", abs_path);
            }
        },
        }
    },
    SimpleFileType::Reg =>
    {
        debug!("WRITE_FILE: {:?}", abs_path);

        // TODO compare file size and mtime? Not needed, the client does
        // that for now

        // Create a temporary file name
        let mut tmp_path: OsString = abs_path.as_os_str().to_owned();
        tmp_path.push(".filesyncd-tmp");
        let tmp_path: PathBuf = tmp_path.into();

        trace!("Writing to temporary file: {:?}", tmp_path);

        let mut file_writer = File::create(&tmp_path)?;

        let mut reader = bufreader.take(body_size);      // wrap bufreader

        io::copy(&mut reader, &mut file_writer)?;

        // Not needed here, in individual function, but needed when we
        // were in the loop in serve(). WHY?
        //bufreader = reader.into_inner();           // give bufreader back

        // File write was a success: rename temporary file
        trace!("Success! Last step: renaming temporary file to destination");
        fs::rename(&tmp_path, &abs_path)?;
    },
    }

    // If a file was not read
    // TODO io::sink() body_size bytes

    // TODO send OK_RESPONSE

    trace!("Finished WRITE_FILE");
    Ok(())
}
