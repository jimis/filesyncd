#![allow(unused_macros)]
#![allow(unused_imports)]

extern crate log;
extern crate env_logger;
use log::{error, warn, info, debug, trace};                     // macro

extern crate serde;
extern crate serde_json;
use serde::{Serialize,Deserialize};

use std::io::Read;
use std::io::Write;
use std::io::BufRead;
use std::iter::Iterator;
use std::os::unix::fs::PermissionsExt;

use std::fs;
use std::fs::File;
use std::path;
use std::path::Path;
use std::path::PathBuf;
use std::time;
use std::net;
use std::net::TcpListener;
use std::net::TcpStream;
use std::net::SocketAddr;
use std::process::exit;
use std::io;
use std::io::BufReader;
use std::collections::HashMap;
use std::error::Error;
use std::ffi::OsString;




#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub enum SimpleFileType {Reg,Dir,Sym,Other}

impl From<std::fs::FileType> for SimpleFileType
{
    fn from(ft: std::fs::FileType) -> SimpleFileType
    {
        if      ft.is_file()    { SimpleFileType::Reg }
        else if ft.is_dir()     { SimpleFileType::Dir }
        else if ft.is_symlink() { SimpleFileType::Sym }
        else                      { SimpleFileType::Other }
    }
}


#[derive(Debug, Serialize, Deserialize)]
pub struct FileDetails {
    pub kind:  SimpleFileType,
    pub depth: i64,
    pub mode:  u32,
    pub mtime: time::SystemTime,
    pub size:  u64,
    pub path:  PathBuf,
}



/**
 *  Returns a recursive directory listing.
 *
 *  NOTE: All paths returned are relative to @path.
 *  NOTE: It never goes out of the given @path.
 */
pub fn calculate_filelist(path: &Path) -> Result<Vec<FileDetails>, std::io::Error>
{
    let mut filelist: Vec<FileDetails> = Vec::new();

    ls_recurse(path, &mut filelist, 0)?;

    // I use while loop in order to delete while iterating
    let mut i = 0;
    while i < filelist.len()
    {
        let fd = &mut filelist[i];
        if fd.path.starts_with(path)
        {
            fd.path = fd.path.strip_prefix(path).unwrap().to_path_buf();
            assert!( fd.path.is_relative() );
            i += 1;
        }
        else
        {
            warn!("A path was found to be outside the path (HOW?), \
                   we will remove it: {}", fd.path.to_string_lossy());
            filelist.remove(i);
        }
    }

    return Ok(filelist);
}


fn ls_recurse(path: &Path, v: &mut Vec<FileDetails>, depth: isize)
    -> Result<(), std::io::Error>
{
    let entries: fs::ReadDir  = fs::read_dir(path)?;

    for entry in entries
    {
        let e: fs::DirEntry  = entry?;

        // Not really needed, read_dir() seems to skip these.
        if e.file_name() == "." || e.file_name() == ".."
        {
            continue;
        }

        let new_path : PathBuf  =
            [path, &PathBuf::from(e.file_name())] .iter().collect();

        match e.file_type()
        {
        Err(error) => error!("Error in file_type('{}'): {:?}",
                             new_path.to_string_lossy(), error),
        Ok(ft) =>
        {
            // safe; does not follow symlinks, as opposed to path.metadata()
            let attr = e.metadata()?;

            let fd = FileDetails {
                kind:   ft.into(),
                depth:  depth as i64,
                path:   new_path.clone(),
                size:   attr.len(),
                mtime:  attr.modified().expect("mtime not supported!!?"),
                mode:   attr.permissions().mode(),
            };

            if ft.is_dir()
            {
                // List the directory *before* entering it, so that it is
                // created first, on the destination.
                v.push(fd);
                ls_recurse(&new_path, v, depth+1)?;
            }
            else if ft.is_file()
            {
                v.push(fd);
            }
            else
            {
                warn!("Skipping unsupported file type {:?} for: {}",
                      fd.kind, new_path.to_string_lossy());
            }
        },
        }
    }
    return Ok(());
}


pub fn is_path_safe(p: &Path) -> bool
{
    use std::mem::discriminant;

    if ! p.is_relative()
    {
        return false;
    }
    // Find if path contains non-normal path components like ".." and "."
    if p.components().find(|&x|
            discriminant(&x) !=
            discriminant(&path::Component::Normal("".as_ref())))
        != None
    {
        return false;
    }

    return true;
}

#[test]
pub fn test_is_path_safe()
{
    assert!(is_path_safe("".as_ref()));
    assert!(is_path_safe("ab".as_ref()));
    assert!(is_path_safe("ab/cd".as_ref()));
    assert!(is_path_safe(".ab".as_ref()));
    assert!(is_path_safe(".ab/".as_ref()));
    assert!(is_path_safe(".ab/".as_ref()));
    assert!(is_path_safe("ab/.c".as_ref()));

    assert!(! is_path_safe("/".as_ref()));
    assert!(! is_path_safe("/ab".as_ref()));
    assert!(! is_path_safe(".".as_ref()));
    assert!(! is_path_safe("..".as_ref()));
    assert!(! is_path_safe("../".as_ref()));
    assert!(! is_path_safe("../ef".as_ref()));
    // Does not occur on Unix according to docs
    //assert!(! is_path_safe("C:/ab".as_ref()));
    // The dot component is completely skipped while iterating over components!
    //assert!(! is_path_safe("ab/.".as_ref()));
    //assert!(! is_path_safe("ab/./cd".as_ref()));
    assert!(! is_path_safe("ab/..".as_ref()));
    assert!(! is_path_safe("ab/../".as_ref()));
    assert!(! is_path_safe("ab/../c".as_ref()));
}



fn map_insert_header(map: &mut HashMap<String,String>, header_line: &[u8])
{
    let header_line = match std::str::from_utf8(header_line)
    {
        Err(_) => return,                               // skip invalid header
        Ok(s)  => s.to_string(),
    };

    // index of first colon ':'
    let i: usize = match header_line.find(':')
    {
        None    => return,                              // skip invalid header
        Some(i) => i,
    };

    let key   = header_line[   ..i].trim().to_string().to_lowercase();
    let value = header_line[i+1.. ].trim().to_string();

    map.insert(key, value);
}

#[allow(non_snake_case)]
pub fn get_headers(stream_buf: &mut BufRead)
    -> Result<HashMap<String,String>, Box<dyn Error>>
{
    let mut map: HashMap<String,String>  = HashMap::new();
    // Buffer to read each line into
    let mut buf: Vec<u8> = Vec::new();

    let mut found_CRLF  : bool  = false;
    let mut double_CRLF : bool  = false;

    while ! double_CRLF
    {
        let next_newline = stream_buf.read_until(b'\n', &mut buf).unwrap();
        // trace!("stream.read_until()={} buf.len={}", next_newline, buf.len());

        if next_newline == 0                                    // EOF
        {
            // Return error
            Err("Error: Incomplete headers received, \
                 maybe peer hung up the connection?") ?;
        }
        else if next_newline == 1                 // empty line no CR, just LF
        {
            assert_eq!(buf[0], b'\n');
            // Just skip bad header line
        }
        else if buf[next_newline - 2] == b'\r'
        {
            assert!(next_newline >= 2);
            assert_eq!(buf[next_newline - 1], b'\n');
            if next_newline == 2                                // empty line
                && found_CRLF                         // previously found CRLF
            {
                double_CRLF = true;
                trace!("End of headers detected");
            }
            found_CRLF = true;
            map_insert_header(&mut map, &buf);
            buf.clear();
        }
        else                         // header line ending with no CR, just LF
        {
            // Just skip bad header line
        }
    }

    Ok(map)
}
