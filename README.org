* Directory sync utility written in Rust

For now this is just a proof of concept offering very basic functionality. It
is my first sizeable program in Rust, and serves mostly as a way to learn the
language.

** Usage

Usage is possible in two modes, either "source" or "destination" mode:


**** Source mode
Send contents of directory to the given address.
This is the default mode.

: filesyncd [--mode source] /path/to/dir 1.2.3.4[:9493]


**** Destination mode
Listen to the given address (127.0.0.1 by default), receive files,
write them to directory.

: filesyncd --mode destination /path/to/dir [1.2.3.4[:9493]]


**** Options:
+  =-h=  =--help=     Print this help message and exit
+  =-M=  =--mode=     { d|dst|dest|destination|s|src|source }
+  =-v=  =--verbose=  increase verbosity; use twice for more.



** License

Copyright (C) 2019 Dimitrios Apostolou

Licensed under GNU Affero GPL license version 3 or later. See file COPYING
or visit <http://www.gnu.org/licenses/agpl-3.0.html>.

